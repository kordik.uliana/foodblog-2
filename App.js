import 'react-native-gesture-handler';
import React, { useState, useEffect } from 'react';
import { StyleSheet, ActivityIndicator, View } from 'react-native';
import * as Font from 'expo-font';
import Navigate from './commponents/navigate';
import { NavigationContainer } from '@react-navigation/native';
import { CartProvider } from "./commponents/CartContext";
import {LoginProvider} from './commponents/LoginContext';
import {SelectedProvider} from './commponents/SelectedContext';


const Fonts = () => Font.loadAsync({
  'mono-bold': require('./assets/fonts/IBMPlexMono-Bold.ttf'),
  'mono-light': require('./assets/fonts/IBMPlexMono-Light.ttf'),
  'barlow': require('./assets/fonts/Barlow-ExtraLight.ttf')
});

export default function App() {
  const [font, setFont] = useState(false);

  useEffect(() => {
    const loadResources = async () => {
      try {
        await Fonts();
        setFont(true);
      } catch (error) {
        console.error('Error loading fonts:', error);
      }
    };

    loadResources();
  }, []);

  if (!font) {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
      </View>
    );
  }

  return (
    <SelectedProvider>
      <LoginProvider>
        <CartProvider>
          <NavigationContainer>
            <Navigate />
          </NavigationContainer>
        </CartProvider>
      </LoginProvider>
    </SelectedProvider>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});