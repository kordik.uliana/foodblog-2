import React from 'react';
import {
    Dimensions,
    View,
    StyleSheet,
} from 'react-native';
import Carousel from 'react-native-reanimated-carousel';
import { Categories, sliderData } from '../data';
import BannerSlider from './BannerSlider';





const HomeCarousel = ({ onCategorySelected, selectedCategory, searchText }) => {
    // запоняем массив Categories отфильтрованным массивом sliderData по категориям, содержащим блюда, соответствующие поисковому запросу
    const filteredCategories = Categories.filter(category => {
        const categoryDishes = sliderData.filter(dish =>
            dish.name.toLowerCase().includes(searchText.toLowerCase()) && dish.category === category.category
            || dish.full.toLowerCase().includes(searchText.toLowerCase()) && dish.category === category.category
            || dish.review.toLowerCase().includes(searchText.toLowerCase()) && dish.category === category.category//dish.category === category.category - находим соотвецтвие между массивом Categories и sliderData
        );
        return categoryDishes.length > 0;//вывод будет происходить когда в массиве есть хотя бы одно блюдо 
    });

    if (!filteredCategories.some(category => category.category === selectedCategory)) {
        onCategorySelected('All');
    }
 
        const renderBanner = ({ item }) => {
        return (
            <BannerSlider
                data={item}
                onSelectCategory={onCategorySelected}
                selectedCategory={selectedCategory}
                
            />
        );
    }

    const loopEnabled = filteredCategories.length > 4;//когда категории больше 3 возращает true(прокрутка работает), меньше или равно 3 - false (прокрутка не работает) 
    const width = Dimensions.get('window').width;
    const baseOptions = {
        width: width / 4,
        height: width / 3,
        style: {
            width:width

        },
    }
    return (
        <View style={styles.carouselContainer}>
            {/* Отображаем карусель только с отфильтрованными категориями */}
            <Carousel
                {...baseOptions}
                loop={loopEnabled}//динамичный выбор прокрутки 
                data={filteredCategories}
                renderItem={renderBanner}
            />
        </View>
    );
}

export default HomeCarousel;


const styles = StyleSheet.create({
    
    carouselContainer: {
        // backgroundColor:'blue',
        marginVertical: 10, // Примерный отступ сверху и снизу
        marginBottom:120
    },
});