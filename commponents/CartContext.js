import React, { createContext, useState } from 'react';

const CartContext = createContext();


export const CartProvider = ({ children }) => { // принимаем дочерние компоненты
  const [cartItems, setCartItems] = useState([]);// состояни емассива данных
  const [totalPrice, setTotalPrice] = useState(0);//полная цена 
  const [isCartVisible, setIsCartVisible] = useState(false);//видимость корзины 
  const [count, setCount] = useState(0);

  const addToCart = (newDishes) => {//добавляем блда в массив 
    const updatedCart = [...cartItems, ...newDishes];
    const summedDishes = updatedCart.reduce((acc, dish) => {//суммирования элементов корзины на основе их имен.
      if (acc[dish.name]) {
        acc[dish.name].count += dish.count;
      } else {
        acc[dish.name] = { ...dish }; // Создаем новый объект с копией свойств текущего блюда
      }
      return acc;
    }, {});

   
    const resultArray = Object.values(summedDishes);

    setCartItems(resultArray);
    setTotalPrice(calculateTotalPrice(updatedCart));
    setIsCartVisible(true)
    setCount(newArrayTotalCount(updatedCart))

    

    // console.log('cartItems',cartItems, 'updatedCart',updatedCart)
  };
  console.log('cartItems',cartItems,'totalPrice',totalPrice)

  const removeFromCart = (index) => {//удаляем блюда 
    const updatedCart = [...cartItems];
    updatedCart.splice(index, 1);
    setCartItems(updatedCart);
    setTotalPrice(calculateTotalPrice(updatedCart));
    setCount(newArrayTotalCount(updatedCart))
  };

  

  const calculateTotalPrice = (items) => {//считаем товар 
    return items.reduce((total, item) => total + (item.cost * item.count) , 0).toFixed(2);
  };
  const newArrayTotalCount = (items) => {
    return items.reduce((total, item) => total + item.count, 0);
  };


  const contextValue = {//передача 
    cartItems,
    totalPrice,
    addToCart,
    removeFromCart,
    isCartVisible,
    setCartItems,
    setTotalPrice,
    count,
    setCount,
    setIsCartVisible,
 
    
  };

  

  return (//передача в дочерние компоненты 
    <CartContext.Provider value={contextValue}>
      {children}
    </CartContext.Provider>
  );
};

export default CartContext;