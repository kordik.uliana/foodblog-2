import React, { useState } from 'react';
import { SafeAreaView, StyleSheet, TextInput, TouchableOpacity, Animated, Easing } from 'react-native';
import { AntDesign } from '@expo/vector-icons';


export default function Input({ onSearch, onCategorySelected,selectedCategory }) {//получаем пропс с содержимым тексом 

  const handleInputChange = (text) => {//введеный текс 
    setTextInput(text);//меняем состояние ввода текста 
    onSearch(text); // Вызываем колбэк при изменении текста ввода
   
  };


  const [search, setSearch] = useState(false); // отвечает за состояние икноки, по умолчанию закрыто 
  const [textInput, setTextInput] = useState('')//хранение ведденого текста 

  const [animation] = useState(new Animated.Value(0));

  const toggleSearch = () => {
    setSearch(!search);// активно/не активно 

    Animated.timing(animation, {
      toValue: search ? 1 : 0,
      duration: 600, // Устанавливаем продолжительность анимации в 200 миллисекунд
      easing: Easing.linear, // Устанавливаем линейную функцию ускорения (анимация будет равномерной)
      useNativeDriver: false, // Отключаем использование нативного драйвера анимации (так как для анимации размера требуется JS)
    }).start(); // Запускаем анимацию
  }
  return (
    <SafeAreaView style={styles.container} >

      <TouchableOpacity onPress={toggleSearch}>
        <Animated.View>
          <AntDesign name="search1" size={24} color="black" />
        </Animated.View>
      </TouchableOpacity>
      {search && (
        <TextInput
          style={styles.input}
          placeholder="Search.."
          value={textInput}
          onChangeText={handleInputChange}
          onFocus={() => {
            setSearch(true);
          }}
        />
      )}
      {search && (
        <AntDesign name="close" size={20} color="black" style={{ padding: 5 }} onPress={() => {
          setTextInput(null);
        }} />)}



    </SafeAreaView>
  );
}
const styles = StyleSheet.create(
  {
    container: {
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: '#fff',
      borderRadius: 20,
      marginHorizontal: 80,


    },
    iconContainer: {
      alignItems: 'center',
    },
    input: {
      flex: 1,
      marginLeft: 10,

    },
  });

