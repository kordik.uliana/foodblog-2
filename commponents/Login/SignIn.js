import React, { useState, useContext } from 'react';
import { View, Text, SafeAreaView, TextInput, StyleSheet, TouchableOpacity } from 'react-native';
import LoginContext from '../LoginContext';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function SingUp() {
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');
    const { setSignedIn, setToken} = useContext(LoginContext)
    const [error, setError] = useState('');

    const handleLogin = async () => {
        try {
            const response = await fetch('https://reqres.in/api/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ password: password, email: email })
            });

            if (response.ok) {
                const data = await response.json();
                console.log('Token:', data.token);
                await AsyncStorage.setItem('authToken', data.token);// сохраняем в хранилище токен
                setToken(data.token)
                setSignedIn(true);

            } else {
                setError('Неверные учетные данные');
            }
        } catch (error) {
            console.error(error);
            setError('Something went wrong');
        }
    };





    return (
        <SafeAreaView>
            <View style={styles.containerInput}>

                <View style={styles.inputContainer}>
                    <Text style={styles.label}>E-mail</Text>
                    <TextInput
                        style={styles.input}
                        placeholder=""
                        value={email}
                        onChangeText={setEmail}
                        keyboardType=""
                    />
                </View>

                <View style={styles.inputContainer}>
                    <Text style={styles.label}>Password</Text>
                    <TextInput
                        style={styles.input}
                        placeholder=""
                        value={password}
                        onChangeText={setPassword}
                        secureTextEntry
                    />
                </View>
                {error ? <Text style={styles.errorText}>{error}</Text> : null}
            </View>

            <TouchableOpacity style={styles.button} onPress={handleLogin}>
                <Text style={styles.buttonTextRegister} >Sing In</Text>
            </TouchableOpacity>

        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    errorText: {
        color: 'red',
        marginLeft:100


    },
    button: {
        backgroundColor: '#F04840',
        paddingVertical: 10,
        marginHorizontal: 65,
        borderRadius: 10,
        marginTop: 20
    },

    buttonTextRegister: {
        fontSize: 17,
        color: '#ffff',
        fontWeight: '500',
        flex: 1,
        justifyContent: 'center',
        textAlign: 'center'
    },
    label: {
        opacity: 0.6,
        fontSize: 12,
        position: 'absolute',
        top: 5,
        left: 75,
        zIndex: 2
    },
    containerInput: {
        marginTop: 30,
        flexDirection: 'column',
        gap: 30
    },
    input: {
        marginHorizontal: 65,
        borderRadius: 10,
        paddingBottom: 5,
        paddingTop: 19,
        paddingHorizontal: 14,
        backgroundColor: '#E1ADA2'
    },

    text: {
        fontSize: 20,
        fontWeight: '600'
    },
    buttonText: {
        marginLeft: 70, marginTop: 200, flexDirection: 'row', gap: 50,
    },

    imageContainer: {
        marginTop: 50,
        marginLeft: 20
    },

});