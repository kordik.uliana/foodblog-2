import React, { useEffect, useState, useContext } from 'react';
import { View, Text, SafeAreaView, TouchableOpacity, StyleSheet, Image, ActivityIndicator } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import PersonalData from './Account/PersonalData';
import SiteData from './Account/SiteInformation';
import LoginContext from '../LoginContext';

export default function Account({ navigation }) {
    const [isDataLoaded, setIsDataLoaded] = useState(false);
    const [isPersonalDataActive, setIsPersonalDataActive] = useState(true);
    const { user, setUsers } = useContext(LoginContext);

    //----------------------------------------
    const fetchUserInfo = async () => {
        const response = await fetch('http://dummyjson.com/users/1');
        const json = await response.json();
        setUsers(json);
        setIsDataLoaded(true);
    };

    //----------------------------------------
    useEffect(() => {
        fetchUserInfo();
    }, []);

    //----------------------------------------
    if (!isDataLoaded) {
        return (
            <View style={[styles.loadingContainer, styles.horizontal]}>
                <ActivityIndicator size="large" />
            </View>
        );
    }

    //----------------------------------------
    const handlePersonalDataPress = () => {
        if (!isPersonalDataActive) {
            setIsPersonalDataActive(true);
        }
    };

    const handleSiteInfoPress = () => {
        if (isPersonalDataActive) {
            setIsPersonalDataActive(false);
        }
    };

    //---------------------------------------
    console.log('user', user);

    return (
        <SafeAreaView>
            <TouchableOpacity style={styles.backButton} onPress={() => navigation.goBack()}>
                <AntDesign name="left" size={24} color="black" />
            </TouchableOpacity>

            <View style={styles.headerContainer}>
                <Text style={styles.headerText}>My Profile</Text>
            </View>

            <Image style={styles.avatar} source={{ uri: 'https://i.pinimg.com/236x/ba/ca/45/baca45133ca61d9a6f9f41a6374d2136.jpg' }} />

            <View style={styles.userNameContainer}>
                <Text style={styles.userFirstName}>{user.firstName}</Text>
                <Text style={styles.userLastName}>{user.lastName}</Text>
            </View>

            <View style={styles.buttonContainer}>
                <TouchableOpacity onPress={handlePersonalDataPress}>
                    <Text style={[styles.buttonText, isPersonalDataActive && styles.activeButtonText]}>Personal data</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={handleSiteInfoPress}>
                    <Text style={[styles.buttonText, !isPersonalDataActive && styles.activeButtonText]}>Info</Text>
                </TouchableOpacity>
            </View>

            {isPersonalDataActive ? (
                <PersonalData />
            ) : (
                <SiteData />
            )}
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    loadingContainer: {
        flex: 1,
        justifyContent: 'center',
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10,
    },
    activeButtonText: {
        position: 'relative',
        zIndex: 2,
        backgroundColor: 'red',
        borderRadius: 10,
    },
    buttonText: {
        paddingHorizontal: 20,
        fontSize: 20,
        fontWeight: '600',
    },
    buttonContainer: {
        borderRadius: 10,
        padding: 3,
        backgroundColor: 'silver',
        opacity: 0.4,
        marginRight: 35,
        marginLeft: 20,
        alignItems: 'center',
        marginTop: 30,
        flexDirection: 'row',
        gap: 30,
    },
    userLastName: {
        fontSize: 20,
        fontWeight: '600',
    },
    userFirstName: {
        fontSize: 20,
        fontWeight: '600',
    },
    userNameContainer: {
        position: 'absolute',
        left: 170,
        top: 160,
        flexDirection: 'row',
        gap: 10,
    },
    backButton: {
        marginTop: 50,
        marginLeft: 20,
    },
    headerContainer: {
        position: 'absolute',
        top: 49,
        left: 150,
    },
    headerText: {
        fontSize: 20,
        fontWeight: '600',
    },
    avatar: {
        marginTop: 40,
        marginLeft: 20,
        borderRadius: 70,
        width: 120,
        height: 120,
    },
});
