import React, {useContext} from 'react';
import { View, Text, StyleSheet, SafeAreaView } from 'react-native';
import LoginContext from '../../LoginContext';

export default function Feed() {
    const { user, setUsers } = useContext(LoginContext)
    return (
        <SafeAreaView style={styles.containerData} >
            <Text style={styles.textEmail} >Email</Text>
            <Text style={styles.textEmailApi} >{user.email}</Text>
            <Text style={styles.textEmail} >Phone</Text>
            <Text style={styles.textEmailApi} >{user.phone}</Text>
            <Text style={styles.textEmail} >Gender</Text>
            <Text style={styles.textEmailApi} >{user.gender}</Text>
            <Text style={styles.textEmail} >Location</Text>
            <Text style={styles.textEmailApi} >{user.address.country}, {user.address.city}, {user.address.address}, {user.address.postalCode}  </Text>
            

        </SafeAreaView>
    );
}
const styles = StyleSheet.create({
    textEmailApi:{
        color:'black',
        marginTop:-15
    },
    containerData: {
        marginLeft:30,
        marginTop:30,
        flexDirection: 'column',
        gap: 30
    },

    textEmail: {
        fontSize: 16,
        opacity: 0.5
    },

    textPhone: {
        fontSize: 16,
        opacity: 0.5
    },
    textPassword: {
        fontSize: 16,
        opacity: 0.5
    },



});