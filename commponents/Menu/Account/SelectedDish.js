import React, { useContext, useEffect, useState } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TouchableOpacity, FlatList, Image, Modal, Pressable } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { AntDesign } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
import SelectedContext from '../../SelectedContext';


export default function SelectedDishScreen() {
  const [modalVisible, setModalVisible] = useState(false);
  const { selectedDishes, removeFromSelectedDishes } = useContext(SelectedContext);
  const [selectedIndex, setSelectedIndex] = useState(null);
  const [isNoFavoriteDish, setIsNoFavoriteDish] = useState(true);
  const navigation = useNavigation();

  //=============================================================
  useEffect(() => {
    if (selectedDishes.length !== 0) {
      setIsNoFavoriteDish(false);
    } else {
      setIsNoFavoriteDish(true);
    }
  }, [selectedDishes]);

  //==============================================================
  const handleDishLongPress = (index) => {
    setModalVisible(true);
    setSelectedIndex(index);
  };

  const handleRemoveDish = () => {
    removeFromSelectedDishes(selectedIndex);
    setModalVisible(false);
  };
  //=============================================================
  console.log('selectedDishes', selectedDishes)

  return (

    <SafeAreaView >


      <Modal
        animationType="none"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Remove a dish from your favorites?</Text>

            <View style={styles.modalButtonRow}>
              <TouchableOpacity
                style={[styles.buttonModal, styles.buttonCloseNo]}
                onPress={() => setModalVisible(!modalVisible)}>
                <Text style={styles.textStyleNo}>No</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.buttonModal, styles.buttonCloseYes]}
                onPress={handleRemoveDish}>
                <Text style={styles.textStyleYes}>Yes</Text>
              </TouchableOpacity>
            </View>

          </View>
        </View>
      </Modal>



      <View style={styles.containerData}>
        <AntDesign name="hearto" size={24} color={'red'} />
        <Text style={styles.textSelected} >Selected Dish</Text>
      </View>



      <FlatList
        contentContainerStyle={{ paddingBottom: 160, paddingTop: 10 }} // Добавляем нижний отступ
        columnWrapperStyle={styles.columnWrapper} // Добавляем стиль обертки для колонок
        data={selectedDishes}
        numColumns={2}
        renderItem={({ item, index }) => (
          <View style={styles.item}>
            <TouchableOpacity onLongPress={() => handleDishLongPress(index)} onPress={() => navigation.navigate('FullInfo', { ...item })}>
              <Image source={{ uri: item.img }} style={{ width: '100%', height: 170, borderRadius: 20 }} />
              <Text style={styles.name}>{item.name}</Text>
              <Ionicons name='star' size={10} color='black' style={styles.iconStar}> <Text>{item.review}</Text></Ionicons>
            </TouchableOpacity>
          </View>
        )}
        keyExtractor={item => item.name}
      />



      {isNoFavoriteDish &&
        (<>
          <Image source={{ uri: 'https://cdn-icons-png.flaticon.com/128/1076/1076716.png' }}
            style={styles.imageCart} />
          <Text style={styles.textCart}>This page is empty</Text>
          <TouchableOpacity style={styles.buttonContinue} onPress={() => navigation.navigate('Main')}>
            <Text style={styles.buttonCheckOutText}>Continue shopping</Text>
          </TouchableOpacity>

        </>)

      }

    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  buttonContinue: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    bottom: 120,
    borderRadius: 27,
    backgroundColor: '#F5CF2F',
    padding: 10,
    margin: 300,
    marginHorizontal: 40,
    justifyContent: 'center',  // Center vertically
    alignItems: 'center',

  },
  buttonCheckOutText: {
    fontSize: 18,
    fontWeight: '500',
  },
  textCart: {
    position: 'absolute',
    top: 300,
    left: 60,
    fontSize: 30,
    fontWeight: '600',
    opacity: 0.7
  },
  imageCart: {
    position: 'absolute',
    top: 190,
    left: 130,
    width: 100,
    height: 100
  },


  containerData: {
    flexDirection: 'row',
    marginLeft: 30,
    marginTop: 70,
    gap: 10
  },

  textSelected: {
    marginTop: -3,
    marginBottom: 2,
    fontSize: 25,
  },
  item: {
    marginTop: 10,
    marginBottom: 20,
    width: '50%',
    paddingHorizontal: 10 // Добавляем горизонтальный отступ между элементами
  },
  columnWrapper: {
    justifyContent: 'space-between', // Равномерно распределяем элементы по вертикали
    // Добавляем нижний отступ между строками
  },

  buttonModal: {
    borderRadius: 20,
    padding: 10,
    paddingHorizontal: 50,
    elevation: 2,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,

  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,

  },
  textStyleNo: {
    color: 'black',

    fontWeight: 'bold',
    textAlign: 'center',
  },
  textStyleYes: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
    fontSize: 15,
    fontWeight: 'bold',
  },
  modalButtonRow: {
    flexDirection: 'row',
    gap: 12
  },
  buttonCloseNo: {
    borderWidth: 1,
    backgroundColor: '#ffff'
  },
  buttonCloseYes: {
    backgroundColor: '#F04840',
  }

});