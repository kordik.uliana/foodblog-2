import { useNavigation } from '@react-navigation/native';
import React, { useState, useContext, useEffect } from 'react';
import { View, Text, FlatList, TouchableOpacity, StyleSheet, Image, Modal, Pressable } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import CartContext from '../CartContext';

export default function ShoppingCart() {
  const [selectedIndex, setSelectedIndex] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);
  const navigation = useNavigation();
  const [isEmptyCart, setIsEmptyCart] = useState(true);//видимость корзины 
console.log('selectedIndex',selectedIndex)
  const {
    cartItems,
    totalPrice,
    removeFromCart,
    setCartItems,
    setTotalPrice,
    isCartVisible,
    setCount,
  } = useContext(CartContext);

  
  useEffect(() => {
    if (cartItems.length !== 0) {
      setIsEmptyCart(false);
    } else {
      setIsEmptyCart(true);
    }
  }, [cartItems,]);



  const summedDishes = cartItems.reduce((acc, dish) => {//суммирования элементов корзины на основе их имен.
    if (acc[dish.name]) {
      acc[dish.name].count += dish.count;
    } else {
      acc[dish.name] = { ...dish }; // Создаем новый объект с копией свойств текущего блюда
    }
    return acc;
  }, {});

  const resultArray = Object.values(summedDishes);


  const promptDeleteDish = (index) => {
    setSelectedIndex(index);
    setModalVisible(true);
  };

  const handleDeleteDish = () => {
    if (selectedIndex !== null) {
      removeFromCart(selectedIndex);
      setModalVisible(false);
    }
  };
  const calculateTotalPrice = (items) => {
    return items.reduce((total, item) => total + item.cost * item.count, 0).toFixed(2);
  };

  const newArrayTotalCount = (items) => {
    return items.reduce((total, item) => total + item.count, 0);
  };

  const increment = (index) => {
    const updatedCartItems = [...resultArray];
    updatedCartItems[index].count++;
    setCartItems(updatedCartItems);
    setTotalPrice(calculateTotalPrice(updatedCartItems));
    setCount(newArrayTotalCount(updatedCartItems));
  };




  const decrement = (index) => {
    if (cartItems[index].count > 1) {
      const updatedCartItems = [...resultArray];
      updatedCartItems[index].count--;
      setCartItems(updatedCartItems);
      setTotalPrice(calculateTotalPrice(updatedCartItems));
      setCount(newArrayTotalCount(updatedCartItems));
    }
    else {
      promptDeleteDish(index);
    }
  };




  console.log('cartItemsCart', cartItems,)
  console.log('resultArray', resultArray)
  return (
    <>
      <Modal
        animationType="none"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Delete product?</Text>

            <View style={styles.modalButtonRow}>
              <Pressable
                style={[styles.buttonModal, styles.buttonCloseNo]}
                onPress={() => setModalVisible(!modalVisible)}>
                <Text style={styles.textStyleNo}>No</Text>
              </Pressable>
              <Pressable
                style={[styles.buttonModal, styles.buttonCloseYes]}
                onPress={handleDeleteDish}>
                <Text style={styles.textStyleYes}>Yes</Text>
              </Pressable>
            </View>

          </View>
        </View>

      </Modal>
      <TouchableOpacity style={styles.goBack} onPress={() => navigation.navigate('Main')}>
        <AntDesign name="close" size={24} color="black" />
      </TouchableOpacity>
      <Text style={styles.headerCart}>My Cart</Text>
      {isCartVisible && (<FlatList
        data={resultArray}
        renderItem={({ item, index }) => (
          <TouchableOpacity onPress={() => navigation.navigate('FullInfo', item)} style={styles.imageContainer} >
            <View style={styles.underline}>
              <Image source={{ uri: item.img }} style={styles.image} />
              <View style={styles.positionText}>
                <Text style={styles.nameText}> {item.name}</Text>
                <Text style={styles.costText}> ${item.cost}</Text>
                <Text style={styles.categoryText}> {item.category}</Text>
              </View>

              <View style={styles.containerCall}>
                <TouchableOpacity onPress={() => decrement(index)} style={styles.button}>
                  <AntDesign name="minus" size={15} color="black" />
                </TouchableOpacity>

                <Text style={styles.count}>{item.count}</Text>

                <TouchableOpacity onPress={() => increment(index)} style={styles.button}>
                  <AntDesign name="plus" size={15} color="black" />
                </TouchableOpacity>
              </View>
            </View>
          </TouchableOpacity>
        )}
      />)}
      {isCartVisible &&
        (<>
          <View style={styles.totalCost}>
            <Text style={styles.totaltext}>Total Cost: </Text>
            <Text style={styles.totalCostNumber}>${totalPrice}</Text>
          </View>
          <TouchableOpacity style={styles.buttonCheckOut} onPress={() => navigation.navigate('Pay')}>
            <Text style={styles.buttonCheckOutText}>Checkout</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.buttonGoBack} onPress={() => navigation.navigate('Main')}>
            <Text style={styles.buttonGoBackText}>Continue shopping</Text>
          </TouchableOpacity>
        </>)

      }

      {isEmptyCart &&
        (<>
          <Text style={styles.textCart}>Haven't decided on a dish yet?</Text>
          <TouchableOpacity style={styles.buttonContinue} onPress={() => navigation.navigate('Main')}>
            <Text style={styles.buttonCheckOutText}>Continue shopping</Text>
          </TouchableOpacity>
          <Image source={{ uri: 'https://cdn-icons-png.flaticon.com/128/481/481384.png' }}
            style={styles.imageCart} />
        </>)

      }





    </>
  );
}


const styles = StyleSheet.create(
  {
    buttonContinue:{
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
      bottom: 100,
      borderRadius: 27,
      backgroundColor: '#F5CF2F',
      padding: 13,
      margin: 300,
      marginHorizontal: 40,
      flex: 1,            // Fill the entire available space
      justifyContent: 'center',  // Center vertically
      alignItems: 'center',

    },
    textCart:{
      position: 'absolute',
      top: 290,
      left: 85,
      fontSize:15,
      fontWeight:'600'
    },
    imageCart: {
      position: 'absolute',
      top: 160,
      left: 140,
      opacity: 0.4,
      width: 100,
      height: 100
    },

    goBack: {
      marginTop: 50,
      marginLeft: 340
    },
    headerCart: {
      marginBottom: 10,
      fontSize: 30,
      marginLeft: 20,
      fontWeight: '600'
    },
    image: {
      borderRadius: 5,
      width: 90,
      height: 90,
      marginLeft: 10,
      marginBottom: 20
    },
    underline: {

      marginLeft: 10,
      width: 360,
      marginTop: 20,
      borderBottomWidth: 1,
      borderColor: 'silver'
    },
    positionText: {
      position: "absolute",
      left: 120
    },
    categoryText: {
      marginTop: 34,
      opacity: 0.3,
      fontSize: 12
    },
    nameText: {
      fontWeight: '600',
      fontSize: 18
    },
    costText: {
      marginTop: 2,
      color: '#F04840',
      fontSize: 13,
      opacity: 0.6
    },
    totalCost: {

      paddingTop: 10,
      height: 260,
      flexDirection: 'row',
      gap: 190,
      marginLeft: 20
    },
    totaltext: {
      color: 'silver',
      fontWeight: '400',
      fontSize: 18
    },
    totalCostNumber: {
      fontSize: 18,
      fontWeight: '600'
    },
    buttonCheckOut: {
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
      bottom: 100,
      borderRadius: 20,
      backgroundColor: '#F5CF2F',
      padding: 13,
      marginHorizontal: 30,
      paddingLeft: 120

    },
    buttonCheckOutText: {
      fontSize: 18,
      fontWeight: '500',
    },
    buttonGoBack: {
      bottom: 70,
      marginHorizontal: 30,
      paddingLeft: 100
    },
    buttonGoBackText: {

    },
    containerCall: {
      position: 'absolute',
      top: 50,
      right: 10,
      gap: 10,
      flexDirection: 'row',
    },
    button: {
      borderWidth: 1,
      borderColor: 'silver',
      opacity: 0.2,
      padding: 8,
      borderRadius: 30

    },
    count: {
      color: 'black',
      fontSize: 20,
      marginTop: 2,
    },


    buttonModal: {
      borderRadius: 20,
      padding: 10,
      paddingHorizontal: 50,
      elevation: 2,
    },
    centeredView: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 22,

    },
    modalView: {
      margin: 20,
      backgroundColor: 'white',
      borderRadius: 20,
      padding: 35,
      alignItems: 'center',
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5,

    },
    textStyleNo: {
      color: 'black',

      fontWeight: 'bold',
      textAlign: 'center',
    },
    textStyleYes: {
      color: 'white',
      fontWeight: 'bold',
      textAlign: 'center',
    },
    modalText: {
      marginBottom: 15,
      textAlign: 'center',
      fontSize: 15,
      fontWeight: 'bold',
    },
    modalButtonRow: {
      flexDirection: 'row',
      gap: 12
    },
    buttonCloseNo: {
      borderWidth: 1,
      backgroundColor: '#ffff'
    },
    buttonCloseYes: {
      backgroundColor: '#F04840',
    }
  }

)