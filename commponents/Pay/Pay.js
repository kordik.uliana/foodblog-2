import * as React from 'react';
import { View, Text, ScrollView, TouchableOpacity, StyleSheet } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';

export default function Pay() {
    const navigation = useNavigation();
    return (
        <View >
            <TouchableOpacity style={styles.imageContainer} onPress={() => navigation.goBack()}>
                <AntDesign name="left" size={24} color="black" />
            </TouchableOpacity>
            <View style={{ justifyContent: 'center', alignItems: 'center', marginTop:300 }}>
                <Text>Pay</Text>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({

    imageContainer: {
        marginTop:50,
        marginLeft:20
    },

});