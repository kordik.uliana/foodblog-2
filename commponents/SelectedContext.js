import React, { createContext, useState, useEffect } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

const SelectedContext = createContext();

export const SelectedProvider = ({ children }) => {
    const [selectedDishes, setSelectedDishes] = useState([]);
    const [heartedDishes, setHeartedDishes] = useState({});

    useEffect(() => {
        const loadSelectedDishes = async () => {
            try {
                const jsonValue = await AsyncStorage.getItem('selectedDish');
                const dishes = jsonValue != null ? JSON.parse(jsonValue) : [];
                setSelectedDishes(dishes);

                const heartValue = await AsyncStorage.getItem('heartState')
                const hearts = heartValue != null ? JSON.parse(heartValue) : {};
                setHeartedDishes(hearts)
            } catch (e) {
                console.log('Failed to load selected dishes from AsyncStorage', e);
            }
        };

        loadSelectedDishes();
    }, []);

    const addToSelectedDishes = async (newDishes) => {
        try {
            const updatedSelected = [...selectedDishes, ...newDishes];
            const summedSelected = updatedSelected.reduce((acc, dish) => {
                if (acc[dish.name]) {
                    acc[dish.name].count += dish.count;
                } else {
                    acc[dish.name] = { ...dish };
                }
                return acc;
            }, {});
            const resultArraySelected = Object.values(summedSelected);
            setSelectedDishes(resultArraySelected);

            const jsonValue = JSON.stringify(resultArraySelected);
            await AsyncStorage.setItem('selectedDish', jsonValue);
        } catch (e) {
            console.log('Failed to save selected dishes to AsyncStorage', e);
        }
    };

    const updateHeartedStateForDish = async (dishName, state) => {
        try {
            const updatedHeartState = { ...heartedDishes, [dishName]: state };
            setHeartedDishes(updatedHeartState);

            const jsonValue = JSON.stringify(updatedHeartState);
            await AsyncStorage.setItem('heartState', jsonValue);
        } catch (e) {
            console.log('Failed to save selected dishes to AsyncStorage', e);
        }
    };

    const removeFromSelectedDishes = async (index) => {
        try {
            const updatedSelected = [...selectedDishes];
            const removedDish = updatedSelected[index];
            updatedSelected.splice(index, 1);
            setSelectedDishes(updatedSelected);
            const jsonValue = JSON.stringify(updatedSelected);
            await AsyncStorage.setItem('selectedDish', jsonValue);

            if (!updatedSelected.some(dish => dish.name === removedDish.name)) {
                const updatedHeartState = { ...heartedDishes };
                delete updatedHeartState[removedDish.name];
                setHeartedDishes(updatedHeartState);
                await AsyncStorage.setItem('heartState', JSON.stringify(updatedHeartState));
            }

            console.log('Данные успешно удалены!', index);
        } catch (error) {
            console.error('Не удалось удалить данные:', error);
        }
    };

    const removeDishesWithHeartState = async (nameDish) => {
        const upDateRemoveName = selectedDishes.filter(dish => dish.name !== nameDish);
        setSelectedDishes(upDateRemoveName);

        const jsonValue = JSON.stringify(upDateRemoveName);
        await AsyncStorage.setItem('selectedDish', jsonValue);
    }

    const contextValue = {
        addToSelectedDishes,
        selectedDishes,
        setSelectedDishes,
        removeFromSelectedDishes,
        heartedDishes,
        setHeartedDishes,
        updateHeartedStateForDish,
        removeDishesWithHeartState
    };

    return (
        <SelectedContext.Provider value={contextValue}>
            {children}
        </SelectedContext.Provider>
    );
};

export default SelectedContext;